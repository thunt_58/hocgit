-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 09, 2016 at 09:46 AM
-- Server version: 5.7.13-log
-- PHP Version: 5.6.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thuctap1`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(250) NOT NULL,
  `del_flg` tinyint(1) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `password` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `first_name`, `last_name`, `email`, `address`, `del_flg`, `create_time`, `update_time`, `avatar`, `password`) VALUES
(1, 'Nguyen Hoang Nam   ', 'Nam', 'Nguyen ', 'namnh@gmail.com', 'Ha Noi', 0, 1208472384, 1473408355, 'data/Desert.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(2, 'Nguyen Van Bao', 'Bao', 'Nguyen', 'baonv@gmail.com', 'Bac Ninh', 1, 0, 0, '', ''),
(3, 'Dang Van Hai', 'Hai', 'Dang', 'haidv@gmail.com', 'Nam Dinh', 1, 0, 0, '', ''),
(4, 'Chu Van Duc', 'Duc', 'Chu', 'duccv@gmail.com', 'Ninh Binh', 1, 0, 0, '', ''),
(5, 'Chu Van Hop', 'Hop', 'Chu', 'hopcv@gmail.com', 'HN', 1, 0, 0, '', ''),
(6, 'Nguyen Hoang Thai', 'Thai', 'Nguyen', 'nguyenthu279.uet@gmail.com', 'BN', 1, 1471444713, 1473006332, 'data/Chrysanthemum.jpg', '1234'),
(10, 'Tran Van Van  ', 'Van', 'Tran', 'vantv@gmail.com', 'Thai Binh', 1, 0, 1471447589, '', '11'),
(14, '11  ', '11', 'qu', 'nsd', 'id', 1, 0, 0, '', ''),
(15, 'Tran Truc Mai ', 'Mai', 'Tran', 'maitt@gmail.com', 'hp', 1, 0, 0, '', ''),
(18, 'Luu van luyen', 'luyen', 'luu', 'QQQQQQQ', 'BN', 1, 0, 0, '', ''),
(21, 'Nguyen Van A', 'A', 'Nguyen', 'anv@gmail.com', 'HN', 1, 0, 0, '', ''),
(53, '', '', '', '', '', 1, 1471409951, 1471409951, '', ''),
(70, 'Vương Tiến Đạt ', 'Vương Tiến', 'Đạt', 'datvt@gmail.com', 'Nam Dinh', 0, 1471444579, 1473405086, 'data/Hydrangeas.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(71, 'Hoang Thanh Binh ', 'Binh', 'Hoang', 'binhht@gmail.com', 'Ninh Binh', 0, 1471444754, 1473405525, 'data/Penguins.jpg', '940adf9c808bdd40930389b5a5a4b29d'),
(72, '  Tran Hoang    ', 'Hoang ', 'Tran ', 'hoangt@gmail.com', 'Hai Phong', 0, 1471447558, 1473394181, 'data/Jellyfish.jpg', '32bb90e8976aab5298d5da10fe66f21d'),
(73, 'Duong Hoang Long  ', 'Long', 'Duong', 'longdh@gmail.com', 'Lang Son', 0, 1471447873, 1473394196, 'data/Koala.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(74, 'Hoang Van Thu      ', 'Thu', 'Hoang', 'thuhv@gmail.com', 'Hung Yen ', 0, 1471482683, 1472128665, 'data/Tulips.jpg', '123'),
(75, 'w', 'q', 'q', 'q', 'q', 1, 1471485128, 1471485139, '', ''),
(76, 'q ', 'q', 'q', 'q', 'q', 1, 1471489661, 1471575938, 'data/IMG_0647.JPG', ''),
(77, '   ', '', '', '', '', 1, 1471490297, 1472123131, 'data/Chrysanthemum.jpg', ''),
(78, ' ', '', '', '', '', 1, 1471492173, 1471575770, 'data/Desert.jpg', ''),
(79, '', '', '', '', '', 1, 1471494421, 1471494421, '', ''),
(80, '  ', '', '', '', '', 1, 1471567575, 1471568077, 'data/Penguins.jpg', ''),
(81, ' ', '', '', '', '', 1, 1471575999, 1471576011, '', ''),
(82, '', '', '', '', '', 1, 1471577317, 1471577317, 'data/IMG_0805.JPG', ''),
(83, '', '', '', '', '', 1, 1472180081, 1472180081, '', ''),
(84, 'Nguyễn Văn Vân ', 'Vân', 'Nguyễn', 'vanvn@gmail.com', 'Bắc Ninh', 1, 1472181141, 1472181141, 'data/Tulips.jpg', '12345'),
(85, ' ', '', '', '', '', 1, 1472187379, 1472187396, 'data/Penguins.jpg', ''),
(86, 'Nguyễn Văn Vân ', 'Vân', 'Nguyễn', 'vantv@gmail.com', 'BN', 1, 1472407784, 1472407784, 'data/Desert.jpg', '123'),
(87, '1231', '321', '321321', '', '', 1, 1472433890, 1472433890, '', ''),
(88, 'Nguyễn Văn Vân ', 'Vân', 'Nguyễn', 'vanvn@gmail.com', 'BN', 0, 1473005805, 1473046654, 'data/Koala.jpg', '12345'),
(89, '', '', '', '', '', 1, 1473005914, 1473005914, 'data/Koala.jpg', ''),
(90, '', '', '', '', '', 1, 1473006124, 1473006566, 'data/Hydrangeas.jpg', ''),
(91, 'Nguyen Hoang Thai', 'Thai', 'Nguyen', 'admin@gmail.com', 'Ha Nam', 0, 1473040122, 1473405124, 'data/Chrysanthemum.jpg', '9fd586679b26dd0677d7b4ce1aa569a9'),
(92, 'Nguyen Hoang Hai', 'Nguyen Hoang', 'Hai', 'hainh@gmail.com', 'HN', 0, 1473040126, 1473403048, 'data/Desert.jpg', '202cb962ac59075b964b07152d234b70'),
(93, '', '', '', '', '', 1, 1473040130, 1473040130, '', ''),
(94, '', '', '', '', '', 1, 1473040135, 1473040135, '', ''),
(110, 'Nguyen Thi Thu', 'Thu', 'Nguyen Thi', 'thunt@gmail.com', 'BN', 0, 1473174983, 1473408495, 'data/Penguins.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(111, 'Nguyen Van A', 'A', 'Nguyen Van ', 'anv@gmail.com', 'HN', 0, 1473174983, 1473414227, 'data/Penguins.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(112, '123', '123', '1231', '1231', '123', 0, 1473174983, 1473396352, 'data/Koala.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(113, '113', '', '', '', '', 0, 1473174983, 1473414245, 'data/Lighthouse.jpg', '41f8df1fca6245eef52593961b4c08f9'),
(114, '113', '', '', '', '', 0, 1473174983, 1473414257, 'data/Desert.jpg', 'ba3e0d6adecc85d83da9fcb42a48470d'),
(115, 'a', 'a', 'a', 'a', 'a', 0, 1473174983, 1473405463, 'data/Desert.jpg', 'c4ca4238a0b923820dcc509a6f75849b'),
(116, '', '', '', '', '', 0, 1473174983, 1473414264, 'data/Chrysanthemum.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(117, '111', '', '', '', '', 0, 1473174983, 1473414273, 'data/Hydrangeas.jpg', '086c80efe4373ec2853ed8740424775b'),
(118, '', '', '', '', '', 0, 1473174984, 1473414282, 'data/Lighthouse.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(119, '', '', '', '', '', 0, 1473174989, 1473414291, 'data/Desert.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(120, '', '', '', '', '', 0, 1473174989, 1473414299, 'data/Koala.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(121, '123', '', '', '', '', 0, 1473174989, 1473414106, 'data/Tulips.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(122, '', '', '', '', '', 1, 1473174989, 1473174989, '', ''),
(123, '', '', '', '', '', 0, 1473174989, 1473414114, 'data/Chrysanthemum.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(124, '', '', '', '', '', 0, 1473174989, 1473414098, 'data/Hydrangeas.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(125, '', '', '', '', '', 1, 1473174989, 1473174989, '', ''),
(126, '', '', '', '', '', 1, 1473174989, 1473174989, '', ''),
(127, '', '', '', '', '', 1, 1473174989, 1473174989, '', ''),
(128, '', '', '', '', '', 1, 1473174989, 1473174989, '', ''),
(129, '', '', '', '', '', 1, 1473175019, 1473175019, '', ''),
(130, '', '', '', '', '', 1, 1473175019, 1473175019, '', ''),
(131, 'root11', '', '', '', '', 0, 1473297773, 1473297773, 'data/Desert.jpg', ''),
(132, '123', '1', '1', '1', '1', 0, 1473347632, 1473347645, 'data/Jellyfish.jpg', '1'),
(133, 'Nguyen Hoang Son', 'Nguyen Hoang', 'Son', 'sonnh@gmail.com', 'Bắc Ninh', 0, 1473388948, 1473388948, 'data/Desert.jpg', '827ccb0eea8a706c4c34a16891f84e'),
(134, 'van', 'van', '1', '1', '1', 0, 1473393434, 1473393480, 'data/Hydrangeas.jpg', '02522a2b2726fb0a03bb19f2d8d952');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
