<?php
    include ("config.php");
?>
<?php
    class suKien{
        public $id;
        public $user_name;
        public $first_name;
        public $last_name;
        public $email;
        public $address;
        public $del_flg;
        public $create_time;
        public $update_time;
        public $avatar;
        public $password;
        public $listSuKien;
        
        function __construct($id='',$user_name='',$first_name='',$last_name='',$email='',$address='',$del_flg='',$create_time='',$update_time='',$avatar='',$password=''){
            $this->id=$id;
            $this->user_name=$user_name;
            $this->first_name=$first_name;
            $this->last_name=$last_name;
            $this->email=$email;
            $this->address=$address;
            $this->del_flg=$del_flg;
            $this->create_time=$create_time;
            $this->update_time=$update_time;
            $this->avatar=$avatar;
            $this->password=$password;
        }
        
        function readFile($fileName){
            $fp = @fopen($fileName, "r");
            $source = null;
  
            // Kiểm tra file mở thành công không
            if (!$fp) {
                echo 'Mở file không thành công';
            }
            else
            {
                $source = fgets($fp);
            }
            fclose($fp);
            return $source;
        }
                
        function getList($start=null, $limit=null){//lấy danh sách các bản ghi
            global $conn;
//            $start = $this->readFile("../ToiTuHocPhp/start.txt");
//            $limit = $this->readFile("../ToiTuHocPhp/limit.txt");
            //$sql = "select * from user where del_flg='0'";
            if ($start || $limit) {
                $rs = mysql_query("SELECT * FROM user where del_flg=0 LIMIT $start, $limit",$conn);
            } else {
                $rs = mysql_query("SELECT * FROM user where del_flg=0");
            }
            //$rs = mysql_query($sql, $conn);
            if(!$rs)
		die($sql);
            $resultlist = array();
            while ($sk = mysql_fetch_array($rs)){        
                $obj = new suKien($sk['id'],$sk['user_name'],$sk['first_name'],$sk['last_name'],$sk['email'],$sk['address'],$sk['del_flg'],$sk['create_time'],$sk['update_time'],$sk['avatar'],$sk['password']);        
                $resultlist[] = $obj;
            }
            $this->listSuKien=$resultlist;
            
        }
        
        function getSuKien(){    //lấy danh sách 1 bản ghi
            global $conn;
            if(isset($_GET)&&($id = @$_GET["id"])){
                $sql = "select * from user where id = '".$id."'";
                $rs = mysql_query($sql, $conn);   
                if($sk = mysql_fetch_array($rs)){
                    $this->id=$sk['id'];
                    $this->user_name=$sk['user_name'];
                    $this->first_name=$sk['first_name'];
                    $this->last_name=$sk['last_name'];
                    $this->email=$sk['email'];
                    $this->address=$sk['address'];
                    $this->del_flg=$sk['del_flg'];
                    $this->create_time=$sk['create_time'];
                    $this->update_time=$sk['update_time'];
                    $this->avatar=$sk['avatar'];
                    $this->password=$sk['password'];
                }     
                else{
                    $this->id='';
                    $this->user_name='';
                    $this->first_name='';
                    $this->last_name='';
                    $this->email='';
                    $this->address='';
                    $this->del_flg='';
                    $this->create_time='';
                    $this->update_time='';
                    $this->avatar='';
                    $this->password='';
                }      
            } 
        }
        function getForm(){//lấy dữ liệu từ form
          //  var_dump($_POST);die;
            $this->id=@$_POST["id"];
            $this->user_name=@$_POST["user_name"];
            $this->first_name=@$_POST["first_name"];
            $this->last_name=@$_POST["last_name"];
            $this->email=@$_POST["email"];
            $this->address=@$_POST["address"];
            $this->del_flg=@$_POST["del_flg"];
            $this->create_time=@$_POST["create_time"];
            $this->update_time=@$_POST["update_time"];
            move_uploaded_file($_FILES['avatar']['tmp_name'],"data/".$_FILES['avatar']['name']);
            if($_FILES['avatar']['name']){        
                $this->avatar="data/".$_FILES['avatar']['name'];
            }
            $this->password=@$_POST["password"];
            
        }
        function getFormLogin(){
          //  var_dump(md5($_POST["password"])); die();
            $this->user_name=@$_POST["user_name"];
            $this->password=@($_POST["password"]);
            //$this->password=  md5($this->password);
        }
        function insert(){
            global $conn;
            $this->getForm();
           // var_dump($_POST);die;
            $sql="insert into user(id,user_name,first_name,last_name,email,address,del_flg,create_time,update_time,avatar,password) values('$this->id','$this->user_name','$this->first_name','$this->last_name','$this->email','$this->address','$this->del_flg','".todbDate($this->create_time)."','".todbDate($this->update_time)."','$this->avatar',md5('$this->password'))";
            $rs=  mysql_query($sql,$conn);
            
            if(!$rs)
                die("Loi truy van".$sql);
            header("Location:index.php?action=view-list");
        }
        function edit(){
            
            global $conn;
            $this->getForm();
            
            if($this->avatar)
                $avatar = "avatar = '$this->avatar',";
            else
                $avatar = '';
            $sql="update user set user_name='$this->user_name',first_name='$this->first_name',last_name='$this->last_name',email='$this->email',"
                    . "address='$this->address',update_time='".todbDate($this->update_time)."',$avatar password=md5('$this->password') where id='$this->id'";
            $rs=  mysql_query($sql,$conn);
            
            if(!$rs)
                die("Loi truy van" .$sql);
             echo "Done";
            // header("Location:index.php?action=view-list");
        }
        
        function delete(){
            global $conn;
            $this->id=$_GET["id"];
            $sql="update user set del_flg='1' where id='$this->id'";
            
            $rs=mysql_query($sql,$conn);
           
            if(!$rs)
                die("Loi truy van".$sql);
            header("Location:index.php?action=view-list");
        }       
        function login(){
            session_start();         
            global $conn;
            if(isset($_POST['login'])){
               $this->getFormLogin();
                if($this->user_name==''||$this->password==''){                 
                    $is_error="Yêu cầu nhập user_name hoặc password";
                }
                else{
                    $sql="select * from user where user_name='$this->user_name'and password=md5($this->password) and del_flg=0";
                    $rs=mysql_query($sql,$conn);
                    $m=mysql_num_rows($rs);
                    if($m==0){
                        $is_error="User_name hoac password chua dung";
                    }
                    else{
                        $_SESSION['user_name']=$this->user_name;
                        header('Location:index.php?action=view-list');
                    }
                }
            }
            include ("view_login.php");
        }
        function logout(){
            session_start(); 
 
            if (isset($_SESSION['user_name'])){
            unset($_SESSION['user_name']); // xóa session login
            }
            header("Location:index.php?action=view_login.php");
        }
        function phantrang(){
            global $conn;
            $sql="select count(*) as total from user where del_flg=0";
            $result = mysql_query($sql,$conn );
            $row = mysql_fetch_assoc($result);
            $total_records = $row['total'];
            $current_page=$_GET['page']? $_GET['page'] : 1;            
            $limit = 10;
            $total_page = ceil($total_records / $limit);           
            if ($current_page > $total_page){
                $current_page = $total_page;
            }
            else if ($current_page < 1){
                $current_page = 1;
            }
            $start = ($current_page - 1) * $limit;            
            $result = mysql_query("SELECT * FROM user where del_flg=0 LIMIT $start, $limit",$conn);
            while ($row =mysql_fetch_assoc($result)){
                $this->getList($start, $limit);
            }            
            if ($current_page > 1 && $total_page > 1){
                echo '<a href="index.php?action=view-list&page='.($current_page-1).'">Prev</a> | ';
            }
            for ($i = 1; $i <= $total_page; $i++){
                // Nếu là trang hiện tại thì hiển thị thẻ span
                // ngược lại hiển thị thẻ a
                if ($i == $current_page){
                    echo '<span>'.$i.'</span> | ';
                }
                else{
                    echo '<a href="index.php?action=view-list&page='.$i.'">'.$i.'</a> | ';
                }
            }
            if ($current_page < $total_page && $total_page > 1){
                echo '<a href="index.php?action=view-list&page='.($current_page+1).'">Next</a> | ';
            }            
        }
    }
?>
