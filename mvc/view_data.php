<?php
    session_start();
    if(!isset($_SESSION['user_name'])){
        header('Location:view_login.php');    
    }
?>
<head>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--Cai dat Bootstrap Modal-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css">

    <style>
        .modal-dialog{
            width: 500px;
            height: 400px;
            background-color: white;
            align-content: center;
        }
    </style>
</head>
<body>   
<h3 align="center">Quản lý thành viên</h3>
<a href="#" data-toggle="modal" data-target="#myModal" >Thêm thành viên</a>
<div id="myModal" class="modal fade" role="dialog"> 
    <div class="modal-dialog">
    <h2>
        Them Thanh Vien
    </h2>
    <form action="index.php?action=_add" method="post" enctype='multipart/form-data'>
        <table>
            
            <tr>
                <td>User_name</td>
                <td><input type="text" name="user_name" size="30"></td>
            
            </tr>
            <tr>
		<td>Avatar</td>
		<td><input type="file" name="avatar" size="30"/></td>
            </tr>
            <tr>
                <td>First_name</td>
                <td><input type="text" name="first_name" size="30"></td>
            </tr>
            <tr>
                <td>Last_name</td>
                <td><input type="text" name="last_name" size="30"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" size="30"></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><input type="text" name="address" size="30"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" size="30"></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" name="submit" value="Add" size="30">
                    <input type="button" name="Cancel" value="Cancel" onclick="history.go( 0);" />
                </td>
            </tr>
            
        </table>
    </form>
    </div> 
</div>

<p><a href='index.php?action=logout'>Đăng suất</a></p>
<table width="90%" class='table' >
<tr>
<th>ID</th>
<th>Avatar</th>
<th>User_name</th> 
<th>First_name</th>
<th>Create_time</th>
<th>Update_time</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
<?php
    foreach($sukienModel->listSuKien as $sukien)
        {
?>
    <tr>
        <td width='30%'><?php echo $sukien->id ;?></td>
        <td><img src="<?php echo $sukien->avatar; ?>" width="60px" height="60px"></td>
        <td><?php echo $sukien->user_name; ?></td>
        <td><?php echo $sukien->first_name; ?></td>
        <td><?php echo toUserDate($sukien->create_time); ?></td>
        <td><?php echo toUserDate($sukien->update_time); ?></td>
        
        
        
        <td onclick="SuaThongTinThanhVien(<?php echo $sukien->id;?>);"><a>Sửa</a></td>
        <td><a href = "#" onClick='if(confirm("Xóa bản ghi?")) window.location = "index.php?action=_del&id=<?php echo $sukien->id ?>";' style='text-decoration:none;'>Xóa</a></td>
    </tr>      
    
<?php
    }
   
?>
</table> 
<br />

<script>
   
    var SuaThongTinThanhVien = function(idMember) {
        // Gửi Request lên server để lấy thông tin KHách hàng theo idMember
        $.get('http://localhost/ToiTuHocPhp/JSON.php?id=' + idMember, function(ketQuaTraVe){
            
            //console.log(ketQuaTraVe);
            // Vì kết quả là dạng JSON được encode về string nên giờ mình cần phải chuyển ngược lại về dạng
            // JSON cho tiện truy vấn thông tin theo các trường của ddooois tượng
            var jsonResult = JSON.parse(ketQuaTraVe);
            // Thuwr kiểm tra
            console.log(jsonResult);
            var formInput = 'MemberID: <p id="member_id">' + jsonResult.id + '</p>';
            /*START_FORM*/
            formInput += '<table >'+
            '<col width="35%" /><col width="65%" />'+
			'<tr>'+
				'<td>User_name</td>'+
                                '<td>'+
                                    '<input id="user_name" type="text" name="user_name" size="30" value="' + jsonResult.user_name + '"/>'+
                                '</td>'+
			'</tr>'+
                        '<tr>'+
				'<td>Avatar</td>'+
				'<td>'+
                                    '<input id="avatar" type="file" name="avatar" size="30"/><img  width="150" src="' + jsonResult.avatar + '"/>'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>First_name</td>'+
                                '<td>'+
                                    '<input id="first_name" type="text" name="first_name"  size="30" value="' + jsonResult.first_name + '">'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Last_name</td>'+
				'<td>'+
                                    '<input id="last_name" type="text" name="last_name" size="30" value="' + jsonResult.last_name + '" />'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Email</td>'+
				'<td>'+
                                    '<input id="email" type="text" name="email" size="30" value="' + jsonResult.email + '" />'+
                               ' </td>'+
			'</tr>'+
			'<tr>'+
				'<td>Address</td>'+
                                '<td>'+
                                    '<input id="address" type="text" name="address" size="30" value="' + jsonResult.address + '">'+
                                '</td>'+
			'</tr>'+
                        '<tr>'+
                                '<td>Password</td>'+
                               '<td>'+
                                    '<input id="password" type="password" name="password" size="30" value="' + jsonResult.password + '">'+
                                '</td>'+
                        '</tr>'+
			'</table>';
            /*END_FORM*/
            // Từ kết quả này mình bắt đầu Build ra cái form Modal
            BootstrapDialog.show({
                title: 'Sửa thông tin thành viên',
                message: formInput,
                
                buttons: [{
                    label: 'Done',
                    action: function(dialog) {
                         // Gọi hàm ajax để lưu dữ liệu tại đây
                         var memberID = $('#member_id').html();
                         var user_name = $('#user_name').val();
                         var first_name = $('#first_name').val();
                         var last_name = $('#last_name').val();
                         var address = $('#address').val();
                         var email = $('#email').val();
                         var password = $('#password').val();
                         var avatar = $('#avatar').attr("src");
                         
                        console.log("Du lieu chinh sua: " + memberID + "|" + user_name + "|" + first_name + "|" + last_name + "|" + address + "|" + password + "|" + email + "|" + avatar);
                         var form = new FormData();
                            form.append('id', memberID); 
                            form.append('user_name', user_name); 
                            form.append('first_name', first_name); 
                            form.append('last_name', last_name); 
                            form.append('email', email); 
                            form.append('address', address); 
                            form.append('password', password); 
                            form.append('avatar', $('input[type=file]')[1].files[0]); 
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=_edit',
                            data: form,
                             contentType: false,
                                processData: false,
                            
                            success: function(kq) {
                                
                            },
                            error:function (xhr){
                                if (xhr.statusText === 'OK') {
                                   console.log("Sửa thành công: ");
                                    dialog.close();
                                    location.reload();
                                }
                             },
                            dataType: 'json'
                           });
                        
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
        })
    }
            
            
            
            
          
</script>

</body>
 