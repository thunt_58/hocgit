<script>
   
    var SuaThongTinKhachHang = function(idCustomer) {
        // Gửi Request lên server để lấy thông tin KHách hàng theo idCustomer
        $.get('http://localhost/ToiTuHocPhp/JSON.php?id=' + idCustomer, function(ketQuaTraVe){
            
            //console.log(ketQuaTraVe);
            // Vì kết quả là dạng JSON được encode về string nên giờ mình cần phải chuyển ngược lại về dạng
            // JSON cho tiện truy vấn thông tin theo các trường của ddooois tượng
            var jsonResult = JSON.parse(ketQuaTraVe);
            // Thuwr kiểm tra
            console.log(jsonResult);
            var formInput = 'CustomerID: <p id="customer_id">' + jsonResult.id + '</p>';
            /*START_FORM*/
            formInput += '<table>'+
            '<col width="35%" /><col width="65%" />'+
			'<tr>'+
				'<td>User_name</td>'+
                                '<td>'+
                                    '<input id="user_name" type="text" name="user_name" size="30" value="' + jsonResult.user_name + '"/>'+
                                '</td>'+
			'</tr>'+
                        '<tr>'+
				'<td>Avatar</td>'+
				'<td>'+
                                    '<input id="avatar" type="file" name="avatar" size="30"/><img  width="150" src="' + jsonResult.avatar + '"/>'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>First_name</td>'+
                                '<td>'+
                                    '<input id="first_name" type="text" name="first_name"  size="30" value="' + jsonResult.first_name + '">'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Last_name</td>'+
				'<td>'+
                                    '<input id="last_name" type="text" name="last_name" size="30" value="' + jsonResult.last_name + '" />'+
                                '</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Email</td>'+
				'<td>'+
                                    '<input id="email" type="text" name="email" size="30" value="' + jsonResult.email + '" />'+
                               ' </td>'+
			'</tr>'+
			'<tr>'+
				'<td>Address</td>'+
                                '<td>'+
                                    '<input id="address" type="text" name="address" size="30" value="' + jsonResult.address + '">'+
                                '</td>'+
			'</tr>'+
                        '<tr>'+
                                '<td>Password</td>'+
                               '<td>'+
                                    '<input id="password" type="password" name="password" size="30" value="' + jsonResult.password + '">'+
                                '</td>'+
                        '</tr>'+
			'</table>';
            /*END_FORM*/
            // Từ kết quả này mình bắt đầu Build ra cái form Modal
            BootstrapDialog.show({
                title: 'Sửa thông tin member',
                message: formInput,
                
                buttons: [{
                    label: 'Done',
                    action: function(dialog) {
                         // Gọi hàm ajax để lưu dữ liệu tại đây
                         var customerID = $('#customer_id').html();
                         var user_name = $('#user_name').val();
                         var first_name = $('#first_name').val();
                         var last_name = $('#last_name').val();
                         var address = $('#address').val();
                         var password = $('#password').val();
                         var email = $('#email').val();
                         var avatar = $('#avatar').attr("src");
                         console.log("Du lieu chinh sua: " + customerID + "|" + user_name + "|" + first_name + "|" + last_name + "|" + address + "|" + password + "|" + email + "|" + avatar);
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=_edit',
                            data: {
                                id: customerID,
                                user_name: user_name,
                                first_name: first_name,
                                last_name: last_name,
                                email: email,
                                address: address,
                                avatar: avatar,
                                password: password
                            },
                            success: function(kq) {
                                
                            },
                            error:function (xhr){
                                if (xhr.statusText === 'OK') {
                                   console.log("Sửa thành công: ");
                                    dialog.close();
                                    location.reload();
                                }
                             },
                            dataType: 'json'
                          });
                        
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
        })
    }
            
            
            